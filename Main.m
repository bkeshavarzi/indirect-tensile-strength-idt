clear
clc

%*********************************************************************************
%**************************** Assign Initial Paramaters *****************

D=150;                                                        % Diameter=150 mm
t=44.5;                                                       % Thickness varies from mixture to mixture (mm)

warning off

Epsilon0=12.5*1e-03/60;
[Node,Element]=ReadMeshGeneratorData();
[Node,Element,Einf,Rho,E,v,a1,a2,a3,af,bf,alpha,T,SlaveDof,MasterDof,BC]=AssignInitials(Node,Element);

Time=linspace(0,2,200)';                                      % Totalormal Analysis Time
dt=Time(2)-Time(1);                                           % Delta increment in time

U=zeros(2*size(Node,1),length(Time));                         % Assign initial size to displacement matrix

RTime=ReducedTime(a1,a2,a3,Time,T);

Direction=zeros(1*size(Element,1),3);                         % Direction for all the gauss points
Direction(:,1)=1;                                             % The angle between local X axis and global X axis
Direction(:,2)=0;                                             % The angle between local Y axis and global Y axis
Direction(:,3)=1;                                             % In plane maximum direction, 1== in-plane, 2== out of plane
Direction(:,4)=1;                                             % The angle between max principal stress and the gloal X axis

DirectionChange=zeros(size(Element,1),1);                     % A vector to indicate first direction change event 

PStressSeries=zeros(3*size(Element,1),length(E));             % The stress  components associated to the pony coefficients, It should be updated and summed up with its previous values
PStressInf=zeros(3*size(Element,1),1);                        % The stress  components associated to the Inf  coefficients, It should be updated and summed up with its previous values
Stress=zeros(3*size(Element,1),length(Time));                 % The current stress value
P=zeros(length(Time),1);                                      % The vector for storing the external force at each time step 
PseudoStrain=zeros(3*size(Element,1),length(Time));           % The vector for storing the pseudo strain value at each time step
ViscosForce=zeros(2*size(Node,1),2);                          % The vector for storing the viscos force components 

Integ1_C=zeros(size(Element,1),length(Time));                 % The matrix for storing cummulative (1-C) during storing

CHistory=zeros(size(Element,1),length(Time));                 % The matrix fro storing C value at each time step 
CHistory(:,1)=Element(:,5);                                   % Assigning first value of C
ER=zeros(size(Element,1),length(Time));                       % The vector for storing maximum principal pseudo strain at each time step 

for iTime=2:length(Time)
    
    [U,PStressSeries,PStressInf,Stress,P,PseudoStrain,Direction,DirectionChange]=GStiffnessMatrix(Node,Element,U,RTime,Time,iTime,Einf,Rho,E,v,Direction,PStressSeries,PStressInf,SlaveDof,MasterDof,Stress,PseudoStrain,P,DirectionChange);  % Making global stiffness matric at each tiem step
    [Element,CHistory,Integ1_C,ER,DirectionChange]=UpdateDamage(Element,Stress,RTime,iTime,af,bf,alpha,Direction,CHistory,PseudoStrain,Integ1_C,ER,DirectionChange);                                                                          % Updating damage at each time step
    fprintf(sprintf('Time is %6.4f, Force is %12.8f \n',Time(iTime),P(iTime)));
    
end