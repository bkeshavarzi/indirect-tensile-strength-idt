function [Element,CHistory,Integ1_C,ER,DirectionChange]=UpdateDamage(Element,Stress,RTime,iTime,af,bf,alpha,Direction,CHistory,PseudoStrain,Integ1_C,ER,DirectionChange)

% This function is responsible for updating damge for each element

for ielem=1:size(Element,1)
    
    if Element(ielem,6) ==0      %If the element has not been already failed
        
        [~,dc]=eig([Stress(3*(ielem-1)+1,iTime) Stress(3*(ielem-1)+3,iTime);Stress(3*(ielem-1)+3,iTime) Stress(3*(ielem-1)+2,iTime)]); %calculating the eignevalue for stress matrix 
        dc=sort(cat(1,0,diag(dc)),'ascend');
        er33=max(dc)/Element(ielem,5);  % ************ calculating maximum pricipal pseudo strain from maximum principal stress componnets **************************

        ER(ielem,iTime)=er33;           % saving maximum pricipal pseudo strain
        
        % Solving Schapery's damage evolution ODE
        
        Tspan=[RTime(iTime-1) RTime(iTime)];
        IC=((-1/af)*log(Element(ielem,5)))^(1/bf);
        [~, Y] = ode15s(@(t,y) myode(t,y,RTime(iTime-1:iTime),[ER(ielem,iTime-1);ER(ielem,iTime)],af,bf,alpha),Tspan,IC); % Solve ODE
        
        %***********************************************************************************************
        % Saving C Params
        Cend=exp(-1*af*(Y(end)^bf)); % Calcuating C value at the end of each step
        Element(ielem,5)=Cend;       % Saving C value 
        
        CHistory(ielem,iTime)=Cend;  % saving C value history
        %***********************************************************************************************
        
        % Calculating required parameters for failure detection
    
        dkesi=diff(RTime(1:iTime))';         %increment in reduced time
        CElem=1-CHistory(ielem,1:iTime);     %1-c
        Summation=cumsum(0.5*(CElem(1:end-1).*dkesi+CElem(2:end).*dkesi));  % integ(1-c)*dt
        Integ1_C(ielem,iTime)=Summation(end); % saving integ(1-c)*dt
        
        if Summation(end) >= 0.0982*RTime(iTime) % Failure detection
            
            CHistory(ielem,iTime)=0;      %if failed, C=0
            Element(ielem,5)=0.001;       %if failed, C would be equal to 0.001 
            Element(ielem,6)=1;           % if failed, sixth column would be to 1
            
        end
        
        %****************************************************************
        
    else  % if element failed beforehand
        
        CHistory(ielem,iTime)=0;
        Element(ielem,5)=0.001;
        Element(ielem,6)=1;
        Integ1_C(ielem,iTime)=Integ1_C(ielem,iTime-1);
        
    end
    
    
end

end


