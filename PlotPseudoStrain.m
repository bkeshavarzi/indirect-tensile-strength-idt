clc
close all

%plot pseudo strain

% it =10;
XP=zeros(3,size(Element,1));
YP=zeros(3,size(Element,1));
PSxx=zeros(size(Element,1),length(Time));
PSyy=zeros(size(Element,1),length(Time));
PSxy=zeros(size(Element,1),length(Time));
PS=zeros(size(Element,1),length(Time));

PseudoStrain_xx=zeros(size(Element,1),length(Time));PseudoStrain_r_xx=zeros(size(Element,1),length(Time));
PseudoStrain_yy=zeros(size(Element,1),length(Time));PseudoStrain_r_yy=zeros(size(Element,1),length(Time));
PseudoStrain_xy=zeros(size(Element,1),length(Time));PseudoStrain_r_xy=zeros(size(Element,1),length(Time));
ERD=zeros(size(Element,1),length(Time));

for it=1:10%iTime
    
    for ielem=1:size(Element,1)
        
        XP(1,ielem)=Node(Element(ielem,2),2);
        XP(2,ielem)=Node(Element(ielem,3),2);
        XP(3,ielem)=Node(Element(ielem,4),2);
        
        YP(1,ielem)=Node(Element(ielem,2),3);
        YP(2,ielem)=Node(Element(ielem,3),3);
        YP(3,ielem)=Node(Element(ielem,4),3);
        
        vec1=[1;0];
        vec2=[Direction(Element(ielem,1),1);Direction(Element(ielem,1),2)];
        TTeta=-1*acos(dot(vec1,vec2)/(norm(vec1,2)*norm(vec2,2)));
        T=[cos(TTeta)^2 sin(TTeta)^2 sin(2*TTeta);sin(TTeta)^2 cos(TTeta)^2 -sin(2*TTeta);-1*sin(TTeta)*cos(TTeta)*0.5 +1*sin(TTeta)*cos(TTeta)*0.5 cos(TTeta)^2-sin(TTeta)^2];
        Temp=(T^-1)*[Stress(3*(ielem-1)+1,it);Stress(3*(ielem-1)+2,it);Stress(3*(ielem-1)+3,it)];
        
        PSxx(ielem,it)=Temp(1);PseudoStrain_xx(ielem,it)=PseudoStrain(3*(ielem-1)+1,it);
        PSyy(ielem,it)=Temp(2);PseudoStrain_yy(ielem,it)=PseudoStrain(3*(ielem-1)+2,it);
        PSxy(ielem,it)=Temp(3);PseudoStrain_xy(ielem,it)=PseudoStrain(3*(ielem-1)+3,it);
        
        PS=sort([PSxx(ielem,it);PSyy(ielem,it);PSxy(ielem,it)],'ascend');
        C0=0.99;
        v=0.3;
        A11=(1/9)*(C0+1*(2*(1+v)/(1-2*v)));
        A22=C0+0.5*((1-2*v)/(1+v));
        A12=(1/3)*(C0-1);
        A44=0.5/(1+v);
        A66=A44;
        Matrix=[A11-(1/3)*A12 A12-(1/3)*A22 -A66;A11-(1/3)*A12 A12-(1/3)*A22 A66;A11+(2/3)*A12 A12+(2/3)*A22 0];
        ER=Matrix^-1*PS;
        ERD(ielem,it)=(1/3)*ER(1)+ER(2);
%       ERD(ielem,it)=PS(3)/C0;
        
        xcenter=mean(XP(1:3,ielem));
        ycenter=mean(YP(1:3,ielem));
        
        vec1=[1;0];
        vec2=[Direction(Element(ielem,1),1);Direction(Element(ielem,1),2)];
        TTeta=-1*acos(dot(vec1,vec2)/(norm(vec1,2)*norm(vec2,2)));
        T=[cos(TTeta)^2 sin(TTeta)^2 sin(2*TTeta);sin(TTeta)^2 cos(TTeta)^2 -sin(2*TTeta);-1*sin(TTeta)*cos(TTeta)*0.5 +1*sin(TTeta)*cos(TTeta)*0.5 cos(TTeta)^2-sin(TTeta)^2];
        Temp=(T^-1)*[PseudoStrain_xx(ielem,it);PseudoStrain_yy(ielem,it);PseudoStrain_xy(ielem,it)];
        
        PseudoStrain_r_xx(ielem,it)=Temp(1);
        PseudoStrain_r_yy(ielem,it)=Temp(2);
        PseudoStrain_r_xy(ielem,it)=Temp(3);
        
    end
    
end

figure

subplot(2,2,1)
title('Principal : S11')
patch(XP,YP,PSxx(:,it)','LineStyle','none')
colorbar
axis equal
xlim([0 0.08])
ylim([-0.08 0.08])
axis off
view(gca,[-90 90]);

subplot(2,2,2)
title('PseudoStrain_xx')
patch(XP,YP,PseudoStrain_xx(:,it)','LineStyle','none')
colorbar
axis equal
xlim([0 0.08])
ylim([-0.08 0.08])
axis off
view(gca,[-90 90]);

subplot(2,2,3)
title('PseudoStrain-r-xx')
patch(XP,YP,PseudoStrain_r_xx(:,it)','LineStyle','none')
colorbar
axis equal
xlim([0 0.08])
ylim([-0.08 0.08])
axis off
view(gca,[-90 90]);

subplot(2,2,4)
title('ER33')
patch(XP,YP,ERD(:,it)','LineStyle','none')
colorbar
axis equal
xlim([0 0.08])
ylim([-0.08 0.08])
axis off
view(gca,[-90 90]);