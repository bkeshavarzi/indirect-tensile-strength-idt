clc
ii=sort(PseudoStrain_r_xx(:,end),'descend');
ind=zeros(length(ii),1);
counter=0;

for i=1:length(ii)
    
    if ii(i) > 0
        counter=counter+1;
        ind(counter)=find(PseudoStrain_r_xx(:,end)==ii(i));
    end
    
end

ind(end:-1:counter+1)=[];
