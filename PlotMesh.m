function [ ] = PlotMesh(Node,Element)
close all

for iNode=1:size(Node,1)
    
    hold on
    plot(Node(iNode,2),Node(iNode,3),'o');
    
end

for iElement=1:size(Element,1)
    
    for iNode=2:4
        
        for jNode=2:4
            
            hold on
            
            X=[Node(Element(iElement,iNode),2) Node(Element(iElement,jNode),2)];
            Y=[Node(Element(iElement,iNode),3) Node(Element(iElement,jNode),3)];
            
            plot(X,Y)
            
        end
        
    end
    
end


end

