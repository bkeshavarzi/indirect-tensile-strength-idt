function [PronyInteg]=EpronyCalculation(DeltaRTime,Einf,Rho,E)

n=size(Rho,1);
PronyInteg=0;

% DeltaTime=-1*DeltaRTime;

for iprony=1:n
    
    PronyInteg=PronyInteg+E(iprony)*exp(-1*DeltaRTime*0.5/Rho(iprony));
    
end

PronyInteg=PronyInteg+Einf;

