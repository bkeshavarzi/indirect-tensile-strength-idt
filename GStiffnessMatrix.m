function [U,PStressSeries,PStressInf,Stress,P,PseudoStrain,Direction,DirectionChange]=GStiffnessMatrix(Node,Element,U,RTime,Time,iTime,Einf,Rho,E,v,Direction,PStressSeries,PStressInf,SlaveDof,MasterDof,Stress,PseudoStrain,P,DirectionChange)

%****************** Defining Global stiffness matrix      *************************************

KG=zeros(2*size(Node,1));

%*****************  Defining force components attributed to viscos material
PForceSeries=zeros(2*size(Node,1),size(E,1));
PForceInf=zeros(2*size(Node,1),1);
ForceSeries=zeros(2*size(Node,1),length(E));
ForceInf=zeros(2*size(Node,1),1);
%*************************************************************************


%*****************  time parameters **************************
TimeStart=RTime(iTime-1);
TimeEnd=RTime(iTime);
TimeMiddle=0.5*(TimeStart+TimeEnd);
DeltaRTime=TimeEnd-TimeStart;
%*************************************************************


%***************** updating stress componets from previous step *********
UpdatingMatrix1=repmat(exp(-1*DeltaRTime./Rho'),3*size(Element,1),1);
UStressSeries=PStressSeries.*UpdatingMatrix1;
UStressInf=PStressInf;
%************************************************************************
t=44.5/1000; % thickness

for ielem=1:size(Element,1)
    
    
    ElementId_Force=[2*Element(ielem,2)-1;2*Element(ielem,2);2*Element(ielem,3)-1;2*Element(ielem,3);2*Element(ielem,4)-1;2*Element(ielem,4)];
    
    TElem=ielem-1;
    ElementId_Stress=[3*TElem+1;3*TElem+2;3*TElem+3];
    
    [A,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y]=ShapeFunctionBuilder(Node,Element(ielem,:),ielem); %***********  Calculating B matrix Componets *************
    
    B=zeros(3,6);       % Initializing B matrix
    
    % *********** Substituting B componnets **************************
    
    B(1,1)=B1_x;B(2,2)=B1_y;B(3,1)=B1_y;B(3,2)=B1_x;
    B(1,3)=B2_x;B(2,4)=B2_y;B(3,3)=B2_y;B(3,4)=B2_x;
    B(1,5)=B3_x;B(2,6)=B3_y;B(3,5)=B3_y;B(3,6)=B3_x;
    
    % ****************************************************************
    
    % *********** Getting Current C value at the gauss point **********
    
    CElement=Element(ielem,5);
    
    % ******************************************************************
    
    % ****************************Constructing Complinace matrix ******
    
    A11=(1/9)*(CElement+1*(2*(1+v)/(1-2*v)));
    A22=CElement+0.5*((1-2*v)/(1+v));
    A12=(1/3)*(CElement-1);
    A44=0.5/(1+v);
    A66=A44;
    
    D11=A11-(2/3)*A12+(1/9)*A22+A66;
    D12=A11-(2/3)*A12+(1/9)*A22-A66;
    D13=A11+(1/3)*A12-(2/9)*A22;
    D21=D12;
    D22=D11;
    D23=A11+(1/3)*A12-(2/9)*A22;
    D31=D13;
    D32=D23;
    D33=A11+(4/3)*A12+(4/9)*A22;
    D44=A66;
    D55=A66;
    D66=A66;
    
    D=zeros(6);
    D(1,1)=D11;D(1,2)=D12;D(1,3)=D13;
    D(2,1)=D21;D(2,2)=D22;D(2,3)=D23;
    D(3,1)=D31;D(3,2)=D32;D(3,3)=D33;
    D(4,4)=D44;D(5,5)=D55;D(6,6)=D66;
    
    invD=D^-1;
    invD(6,:)=[];invD(5,:)=[];invD(1,:)=[];
    invD(:,6)=[];invD(:,5)=[];invD(:,1)=[];
    D=invD^-1;
    % *****************************************************************
    
    % Calcuating Angle between global axis and maximum principal stress
    
    vec1=[1;0];
    vec2=[Direction(Element(ielem,1),1);Direction(Element(ielem,1),2)];
    TTeta=-1*acos(dot(vec1,vec2)/(norm(vec1,2)*norm(vec2,2)));
    
    %******************************************************************
    
    %*********Transforming compliance matrix to the global axis direction
    
    if Direction(Element(ielem,1),3)==1 %
        
        CLocal=zeros(3);
        CLocal(1,1)=D(2,2);
        CLocal(1,2)=D(2,1);
        CLocal(1,3)=0;
        CLocal(2,1)=D(1,2);
        CLocal(2,2)=D(1,1);
        CLocal(2,3)=0;
        CLocal(3,1)=0;
        CLocal(3,2)=0;
        CLocal(3,3)=D(3,3);
        
        
        T=[cos(TTeta)^2 sin(TTeta)^2 sin(2*TTeta);sin(TTeta)^2 cos(TTeta)^2 -sin(2*TTeta);-1*sin(TTeta)*cos(TTeta) +1*sin(TTeta)*cos(TTeta) cos(TTeta)^2-sin(TTeta)^2];
        CBar=T*CLocal*T^-1;
        
    else
        
        CLocal=zeros(3);
        CLocal(1,1)=D(1,1);
        CLocal(1,2)=D(1,2);
        CLocal(2,1)=D(1,2);
        CLocal(2,2)=D(1,1);
        CLocal(3,3)=D(3,3);
        CBar=CLocal;
        
    end
    
    %*****************************************************************
    
    % Obtaining stiffness matrix for element **********************
    
    Kelem=B'*CBar*1*EpronyCalculation(DeltaRTime,Einf,Rho,E)*B*t*A;
    
    %***************************************************************
    
    
    % ************************************ Calculating force/stress components for viscos material ***************
    
    for iprony=1:length(Rho)
        
        ForceSeries(ElementId_Force,iprony)=ForceSeries(ElementId_Force,iprony)-B'*CBar*EpronyCalculationPart(DeltaRTime,Rho(iprony),E(iprony))*B*t*U(ElementId_Force,iTime-1)*A;
        PForceSeries(ElementId_Force,iprony)=PForceSeries(ElementId_Force,iprony)+B'*CBar*1*UStressSeries(ElementId_Stress,iprony)*t*A;
        
    end
    
    for iprony=1:length(Rho)
        
        UStressSeries(ElementId_Stress,iprony)=UStressSeries(ElementId_Stress,iprony)-EpronyCalculationPart(DeltaRTime,Rho(iprony),E(iprony))*B*1*U(ElementId_Force,iTime-1);
        
    end
    
    ForceInf(ElementId_Force,1)=ForceInf(ElementId_Force,1)-B'*CBar*Einf*B*U(ElementId_Force,iTime-1)*t*A;
    PForceInf(ElementId_Force,1)= PForceInf(ElementId_Force,1)+B'*CBar*1*UStressInf(ElementId_Stress,1)*t*A;
    UStressInf(ElementId_Stress,1)=UStressInf(ElementId_Stress,1)-Einf*B*U(ElementId_Force,iTime-1);
    %***************************************************************************************************************
    
    for inode=1:3
        
        for jnode=1:3
            
            NodeIdi=Element(ielem,inode+1);
            NodeIdj=Element(ielem,jnode+1);
            
            KG(2*NodeIdi-1,2*NodeIdj-1) =  KG(2*NodeIdi-1,2*NodeIdj-1) +  Kelem(2*inode-1,2*jnode-1);
            KG(2*NodeIdi-1,2*NodeIdj)   =  KG(2*NodeIdi-1,2*NodeIdj)   +  Kelem(2*inode-1,2*jnode);
            KG(2*NodeIdi,2*NodeIdj-1)   =  KG(2*NodeIdi,2*NodeIdj-1)   +  Kelem(2*inode,2*jnode-1);
            KG(2*NodeIdi,2*NodeIdj)     =  KG(2*NodeIdi,2*NodeIdj)     +  Kelem(2*inode,2*jnode);
            
        end
        
    end
    
end

%  ********************************splitting global stiffness matric according to master/ slave method ***

Kmm=KG(MasterDof,MasterDof);
Kms=KG(MasterDof,SlaveDof);
Ksm=KG(SlaveDof,MasterDof);
Kss=KG(SlaveDof,SlaveDof);

%  **********************************************************************************************************

% **********************************  calculating total force ***********************************************

TotalForce=-1*(sum(PForceSeries,2)+PForceInf);

DeltaUs=-1*(12.5/60)*10^-3*(Time(iTime)-Time(iTime-1));
TotalUS=-1*(12.5/60)*10^-3*Time(iTime);

FExt1=-1*Kms*repmat(DeltaUs,length(SlaveDof),1);
FExt2=TotalForce(MasterDof,1);
FExt=FExt1+FExt2;

%************************************************************************************************************

%**************************  Claculating increment in displacement

dUm=Kmm\FExt;

%************************************************************************************************************

% *****************************************Assigning displacement/force

U(MasterDof,iTime)=dUm+U(MasterDof,iTime-1);
U(SlaveDof,iTime)=TotalUS;
P(iTime,1)=2*sum(Ksm*dUm+Kss*repmat(DeltaUs,length(SlaveDof),1)-TotalForce(SlaveDof,1));

%************************************************************************************************************

% Displacement for current time step is calculated, now stress should be
% calculated and updated
% Most of this section is the same as above

for ielem=1:size(Element,1)
    
    if Element(ielem,6)==0
        
        ElementId_Force=[2*Element(ielem,2)-1;2*Element(ielem,2);2*Element(ielem,3)-1;2*Element(ielem,3);2*Element(ielem,4)-1;2*Element(ielem,4)];
        
        TElem=ielem-1;
        ElementId_Stress=[3*TElem+1;3*TElem+2;3*TElem+3];
        
        [A,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y]=ShapeFunctionBuilder(Node,Element(ielem,:),ielem);
        
        B=zeros(3,6);
        
        B(1,1)=B1_x;B(2,2)=B1_y;B(3,1)=B1_y;B(3,2)=B1_x;
        B(1,3)=B2_x;B(2,4)=B2_y;B(3,3)=B2_y;B(3,4)=B2_x;
        B(1,5)=B3_x;B(2,6)=B3_y;B(3,5)=B3_y;B(3,6)=B3_x;
        
        CElement=Element(ielem,5);
        
        A11=(1/9)*(CElement+1*(2*(1+v)/(1-2*v)));
        A22=CElement+0.5*((1-2*v)/(1+v));
        A12=(1/3)*(CElement-1);
        A44=0.5/(1+v);
        A66=A44;
        D11=A11-(2/3)*A12+(1/9)*A22+A66;
        D12=A11-(2/3)*A12+(1/9)*A22-A66;
        D13=A11+(1/3)*A12-(2/9)*A22;
        D21=D12;
        D22=D11;
        D23=A11+(1/3)*A12-(2/9)*A22;
        D31=D13;
        D32=D23;
        D33=A11+(4/3)*A12+(4/9)*A22;
        D44=A66;
        D55=A66;
        D66=A66;
        
        D=zeros(6);
        D(1,1)=D11;D(1,2)=D12;D(1,3)=D13;
        D(2,1)=D21;D(2,2)=D22;D(2,3)=D23;
        D(3,1)=D31;D(3,2)=D32;D(3,3)=D33;
        D(4,4)=D44;D(5,5)=D55;D(6,6)=D66;
        
        invD=D^-1;
        invD(1,:)=[];
        invD(:,1)=[];
        D=invD^-1;
        
        vec1=[1;0];
        vec2=[Direction(Element(ielem,1),1);Direction(Element(ielem,1),2)];
        TTeta=-1*acos(dot(vec1,vec2)/(norm(vec1,2)*norm(vec2,2)));
        
        
        if Direction(Element(ielem,1),3)==1
            
            CLocal=zeros(3);
            CLocal(1,1)=D(2,2);
            CLocal(1,2)=D(2,1);
            CLocal(1,3)=0;
            CLocal(2,1)=D(1,2);
            CLocal(2,2)=D(1,1);
            CLocal(2,3)=0;
            CLocal(3,1)=0;
            CLocal(3,2)=0;
            CLocal(3,3)=D(3,3);
            
            T=[cos(TTeta)^2 sin(TTeta)^2 sin(2*TTeta);sin(TTeta)^2 cos(TTeta)^2 -sin(2*TTeta);-1*sin(TTeta)*cos(TTeta) +1*sin(TTeta)*cos(TTeta) cos(TTeta)^2-sin(TTeta)^2];
            CBar=T*CLocal*T^-1;
            
        else
            
            CLocal=zeros(3);
            CLocal(1,1)=D(1,1);
            CLocal(1,2)=D(1,2);
            CLocal(2,1)=D(1,2);
            CLocal(2,2)=D(1,1);
            CLocal(3,3)=D(3,3);
            CBar=CLocal;
            
        end
        
        for iprony=1:length(Rho)
            
            UStressSeries(ElementId_Stress,iprony)=UStressSeries(ElementId_Stress,iprony)+EpronyCalculationPart(DeltaRTime,Rho(iprony),E(iprony))*B*1*U(ElementId_Force,iTime);
            PStressSeries(ElementId_Stress,iprony)=UStressSeries(ElementId_Stress,iprony);
            
        end
        
        for iprony=1:length(E)
            
            UStressSeries(ElementId_Stress,iprony)=CBar*UStressSeries(ElementId_Stress,iprony);
            
        end
        
        UStressInf(ElementId_Stress,1)=UStressInf(ElementId_Stress,1)+Einf*B*U(ElementId_Force,iTime);
        PStressInf(ElementId_Stress,1)=UStressInf(ElementId_Stress,1);
        
        UStressInf(ElementId_Stress,1)=CBar*UStressInf(ElementId_Stress,1);
        
        Stress(ElementId_Stress,iTime)=(sum(UStressSeries(ElementId_Stress,:),2)+UStressInf(ElementId_Stress,1));
        
        StressVector=Stress(ElementId_Stress,iTime);
        StressMatrix=[StressVector(1) StressVector(3);StressVector(3) StressVector(2)];
        
        [vec,d]=eig(StressMatrix);
        d=diag(d);
        [val,loc]=max(d);
        
        
        % Dircetion variation is calulated in here, for the sake of
        % reducing computational time, this variation is calcualted only
        % one time
        
        %****************************************************************************************
        
        if  DirectionChange(ielem,1)==0
            
            if val > 0
                
                Direction(Element(ielem,1),3)=1;
                
                if (vec(1,loc)*vec(2,loc) > 0) && (vec(1,loc)<0)
                    
                    Direction(Element(ielem,1),1)=-1*vec(1,loc);
                    Direction(Element(ielem,1),2)=-1*vec(2,loc);
                    
                else
                    
                    Direction(Element(ielem,1),1)=1*vec(1,loc);
                    Direction(Element(ielem,1),2)=1*vec(2,loc);
                    
                end
                
                vec1=[1;0];
                vec2=[Direction(Element(ielem,1),1);Direction(Element(ielem,1),2)];
                TTeta=1*acos(dot(vec1,vec2)/(norm(vec1,2)*norm(vec2,2)));
                Direction(Element(ielem,1),4)=TTeta;
                DirectionChange(ielem,1)=1;
                
            else
                
                Direction(Element(ielem,1),1)=1;
                Direction(Element(ielem,1),2)=0;
                Direction(Element(ielem,1),3)=0;
                Direction(Element(ielem,1),4)=0;
                
            end
            
        end
        
        %*******************************************************************************************************
        %*********************************************Calculating pseudo strain vector
        
        PseudoStrain(ElementId_Stress,iTime)=sum(PStressSeries(ElementId_Stress,:),2)+PStressInf(ElementId_Stress,1);
        
        %*******************************************************************************************************
        
    end
    
    
end

end

