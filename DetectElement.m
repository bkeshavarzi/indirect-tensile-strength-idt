close all
delem=[43,435,438,431,413,874,875,875,876,859,479];%[35,47,137,138,141,147,172,193,199,205,200,206,194,173,148,142,139,140,48,1];%data;%[853,826,855,828,814,815];%[42;43;418;43;124;435];%[434;429;436;432;132;127;1];
dnode1=[10000,20000,50000];
dnode2=400000000;%index;%0.5*(SlaveDof);

for iNode=1:size(Node,1)
    
    hold on
    plot(Node(iNode,2),Node(iNode,3),'o');
    
end

for iElement=1:size(Element,1)
    
    for iNode=2:4
        
        for jNode=2:4
            
            hold on
            
            X=[Node(Element(iElement,iNode),2) Node(Element(iElement,jNode),2)];
            Y=[Node(Element(iElement,iNode),3) Node(Element(iElement,jNode),3)];
            
            plot(X,Y)
            
        end
        
        for in=1:length(dnode1)
            
            if Node(Element(iElement,2),1)==dnode1(in)
                
                hold on
                plot(Node(Element(iElement,2),2),Node(Element(iElement,2),3),'red.-','markersize',20)
                
            elseif Node(Element(iElement,3),1)==dnode1(in)
                
                hold on
                plot(Node(Element(iElement,3),2),Node(Element(iElement,3),3),'red.-','markersize',20)
                
            elseif Node(Element(iElement,4),1)==dnode1(in)
                
                hold on
                plot(Node(Element(iElement,4),2),Node(Element(iElement,4),3),'red.-','markersize',20)
                
            end
            
        end
        
        for in=1:length(dnode2)
            
            if Node(Element(iElement,2),1)==dnode2(in)
                
                hold on
                plot(Node(Element(iElement,2),2),Node(Element(iElement,2),3),'green.-','markersize',20)
                
            elseif Node(Element(iElement,3),1)==dnode2(in)
                
                hold on
                plot(Node(Element(iElement,3),2),Node(Element(iElement,3),3),'green.-','markersize',20)
                
            elseif Node(Element(iElement,4),1)==dnode2(in)
                
                hold on
                plot(Node(Element(iElement,4),2),Node(Element(iElement,4),3),'green.-','markersize',20)
                
            end
            
        end
        
        
    end
    
    for in=1:length(delem)
        if iElement==delem(in)
            
            XP=zeros(3,1);
            YP=zeros(3,1);
            XP(1,1)=Node(Element(iElement,2),2);YP(1,1)=Node(Element(iElement,2),3);
            XP(2,1)=Node(Element(iElement,3),2);YP(2,1)=Node(Element(iElement,3),3);
            XP(3,1)=Node(Element(iElement,4),2);YP(3,1)=Node(Element(iElement,4),3);
            patch(XP,YP,'red')
            
        end
    end
    
    
    
end


vnode=find(Node(:,2)==0);
data=0;

% for ielem=1:size(Element,1)
%     
%     for inode=1:length(vnode)
%         
%         if Element(ielem,2)==vnode(inode)
%             
%             data=cat(1,data,ielem);
%             
%         end
%         
%         if Element(ielem,3)==vnode(inode)
%             
%             data=cat(1,data,ielem);
%             
%         end
%         
%         if Element(ielem,4)==vnode(inode)
%             
%             data=cat(1,data,ielem);
%             
%         end
%         
% %         xcenter=mean(Node(Element(ielem,2:4),2));data(ielem,2)=xcenter;
% %         ycenter=mean(Node(Element(ielem,2:4),3));data(ielem,3)=ycenter;
%         
%     end
%     
%     
% end
% 
% data(1)=[];
% data=unique(data);
% idata=zeros(length(data),2);
% 
% for ielem=1:length(data)
%     
%     idata(ielem,1)=mean(Node(Element(data(ielem),2:4),2));
%     idata(ielem,2)=mean(Node(Element(data(ielem),2:4),3));
%     
% end

    
    