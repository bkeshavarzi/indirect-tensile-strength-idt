close all
XP=zeros(3,size(Element,1));
YP=zeros(3,size(Element,1));
XC=zeros(1,size(Element,1));
YC=zeros(1,size(Element,1));

UV=zeros(1,size(Element,1));
VV=zeros(1,size(Element,1));

UXP=zeros(3,size(Element,1));
UYP=zeros(3,size(Element,1));

Sxx=zeros(size(Element,1),1);
Syy=zeros(size(Element,1),1);
Sxy=zeros(size(Element,1),1);

Exx=zeros(size(Element,1),1);
Eyy=zeros(size(Element,1),1);
Exy=zeros(size(Element,1),1);

PSxx=zeros(size(Element,1),1);
PSyy=zeros(size(Element,1),1);
PSxy=zeros(size(Element,1),1);

C=zeros(size(Element,1),1);
FailureElement=zeros(size(Element,1),1);
MaxPstrain_StressBased=zeros(size(Element,1),1);
MaxPstrain_StrainBased=zeros(size(Element,1),1);
MaxPstrain_InverseStressBased=zeros(size(Element,1),1);

it =147;

for ielem=1:size(Element,1)
    
    XP(1,ielem)=Node(Element(ielem,2),2);
    XP(2,ielem)=Node(Element(ielem,3),2);
    XP(3,ielem)=Node(Element(ielem,4),2);
    
    YP(1,ielem)=Node(Element(ielem,2),3);
    YP(2,ielem)=Node(Element(ielem,3),3);
    YP(3,ielem)=Node(Element(ielem,4),3);
    
    UXP(1,ielem)=U(2*Node(Element(ielem,2),1)-1,it)*1000;
    UXP(2,ielem)=U(2*Node(Element(ielem,3),1)-1,it)*1000;
    UXP(3,ielem)=U(2*Node(Element(ielem,4),1)-1,it)*1000;
    
    UYP(1,ielem)=U(2*Node(Element(ielem,2),1),it)*1000;
    UYP(2,ielem)=U(2*Node(Element(ielem,3),1),it)*1000;
    UYP(3,ielem)=U(2*Node(Element(ielem,4),1),it)*1000;
    
    Sxx(ielem,1)=Stress(3*(ielem-1)+1,it);
    Syy(ielem,1)=Stress(3*(ielem-1)+2,it);
    Sxy(ielem,1)=Stress(3*(ielem-1)+3,it);
    
    
    [A,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y]=ShapeFunctionBuilder(Node,Element(ielem,:),ielem);
    
    B=zeros(3,6);
    B(1,1)=B1_x;B(2,2)=B1_y;B(3,1)=B1_y;B(3,2)=B1_x;
    B(1,3)=B2_x;B(2,4)=B2_y;B(3,3)=B2_y;B(3,4)=B2_x;
    B(1,5)=B3_x;B(2,6)=B3_y;B(3,5)=B3_y;B(3,6)=B3_x;
    
    Epsilon=B*[U(2*Node(Element(ielem,2),1)-1,it);U(2*Node(Element(ielem,2),1),it);U(2*Node(Element(ielem,3),1)-1,it);U(2*Node(Element(ielem,3),1),it);U(2*Node(Element(ielem,4),1)-1,it);U(2*Node(Element(ielem,4),1),it)];
    Exx(ielem,1)=Epsilon(1);
    Eyy(ielem,1)=Epsilon(2);
    Exy(ielem,1)=Epsilon(3)/2;
    
    
    sigma=[Sxx(ielem,1) Sxy(ielem,1);Sxy(ielem,1) Syy(ielem,1)];
    [~,d]=eig(sigma);
    d=diag(d);
    
    PSxx(ielem,1)=PseudoStrain(3*(ielem-1)+1,it);
    PSyy(ielem,1)=PseudoStrain(3*(ielem-1)+2,it);
    PSxy(ielem,1)=PseudoStrain(3*(ielem-1)+3,it);
    Psigma=[PSxx(ielem,1) PSxy(ielem,1);PSxy(ielem,1) PSyy(ielem,1)];
    [~,d2]=eig(Psigma);
    d2=diag(d2);
    
    xcenter=mean(XP(1:3,ielem));
    ycenter=mean(YP(1:3,ielem));
    
    XC(1,ielem)=xcenter;YC(1,ielem)=ycenter;
    UV(1,ielem)=Direction(ielem,1);
    VV(1,ielem)=Direction(ielem,2);
    
    C(ielem,1)=CHistory(ielem,it);
    
    if CHistory(ielem,it) > 0
        FailureElement(ielem,1)=0;
    else
        FailureElement(ielem,1)=1;
    end
    
    
    vec1=[1;0];
    vec2=[Direction(Element(ielem,1),1);Direction(Element(ielem,1),2)];
    TTeta=-1*acos(dot(vec1,vec2)/(norm(vec1,2)*norm(vec2,2)));
    T=[cos(TTeta)^2 sin(TTeta)^2 sin(2*TTeta);sin(TTeta)^2 cos(TTeta)^2 -sin(2*TTeta);-1*sin(TTeta)*cos(TTeta) +1*sin(TTeta)*cos(TTeta) cos(TTeta)^2-sin(TTeta)^2];
    Temp=T*[Stress(3*(ielem-1)+1,it);Stress(3*(ielem-1)+2,it);Stress(3*(ielem-1)+3,it)];
    MaxPstrain_StressBased(ielem,1)=max(d)/Element(ielem,5);
    MaxPstrain_StrainBased(ielem,1)=max(d2);
    
    
    
    v=0.3;
    CElement=Element(ielem,5);
    A11=(1/9)*(CElement+1*(2*(1+v)/(1-2*v)));
    A22=CElement+0.5*((1-2*v)/(1+v));
    A12=(1/3)*(CElement-1);
    A44=0.5/(1+v);
    A66=A44;
    
    Matrix=zeros(3);
    Matrix(1,1)=A11-(1/3)*A12;
    Matrix(1,2)=A12-(1/3)*A22;
    Matrix(1,3)=-1*A66;
    Matrix(2,1)=A11-(1/3)*A12;
    Matrix(2,2)=A12-(1/3)*A22;
    Matrix(2,3)=A66;
    Matrix(3,1)=A11+(2/3)*A12;
    Matrix(3,2)=A12+(2/3)*A22;
    er=Matrix^-1*sort(cat(1,0,d2),'ascend');
    er33=(1/3)*er(1)+er(2);
    er33(er33<0)=0;
    MaxPstrain_InverseStressBased(ielem,1)=er33;
    
end

SpecificExx=Exx(delem);SpecificSxx=Sxx(delem);
SpecificEyy=Eyy(delem);SpecificSyy=Syy(delem);
SpecificExy=Exy(delem);SpecificSxy=Sxy(delem);

% figure
% title('StrainBased')
% patch(XP,YP,MaxPstrain_StrainBased','LineStyle','none')
% colorbar
% axis equal
% xlim([0 0.08])
% ylim([-0.08 0.08])
% axis off
%
% figure
% title('StressBased')
% patch(XP,YP,MaxPstrain_StressBased','LineStyle','none')
% colorbar
% axis equal
% xlim([0 0.08])
% ylim([-0.08 0.08])
% axis off
%
% figure
% title('InverseStressBased')
% patch(XP,YP,MaxPstrain_InverseStressBased','LineStyle','none')
% colorbar
% axis equal
% xlim([0 0.08])
% ylim([-0.08 0.08])
% axis off

figure
title('C')
patch(XP,YP,C','LineStyle','none')
colorbar
axis equal
xlim([0 0.08])
ylim([-0.08 0.08])
caxis([0 1])
title('C','FontSize',18);
colorbar('peer',gca,'FontSize',16);
axis off

figure
title('Failed Element')
patch(XP,YP,FailureElement','LineStyle','none')
colorbar
axis equal
xlim([0 0.08])
ylim([-0.08 0.08])
caxis([0 1])
title('Failed Element','FontSize',18);
colorbar('peer',gca,'FontSize',16);
axis off