function [A,B1_x,B1_y,B2_x,B2_y,B3_x,B3_y]=ShapeFunctionBuilder(Node,Element,iElem)

X1=Node(Element(2),2);Y1=Node(Element(2),3);
X2=Node(Element(3),2);Y2=Node(Element(3),3);
X3=Node(Element(4),2);Y3=Node(Element(4),3);

C=[1 X1 Y1;1 X2 Y2;1 X3 Y3];
A=0.5*det(C);

if A <= 0
    
    disp(sprintf('Error in Calculating Area of Traingle for Element %d',iElem));
    
end

invC=inv(C);

B1_x=invC(2,1);B2_x=invC(2,2);B3_x=invC(2,3);
B1_y=invC(3,1);B2_y=invC(3,2);B3_y=invC(3,3);

end

