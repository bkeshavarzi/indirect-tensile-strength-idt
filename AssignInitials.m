function [Node,Element,Einf,Rho,E,v,a1,a2,a3,af,bf,alpha,T,SlaveDof,MasterDof,BC]=AssignInitials(Node,Element)

Data=xlsread('Vte00.csv');   % Name of the file containing relaxation modulus data
Einf=Data(1,2);               % Einf value, kPa 
Rho=Data(2:end,1);            % Relaxation time, second
E=Data(2:end,2);              % Modulus values, kPa 
v=0.30;                       % Poisson ratio

a1=6.14e-04;                  % time-temperature shift factor, a1
a2=-1.57e-01;                 % time-temperature shift factor, a2
a3=7.72e-01;                  % time-temperature shift factor, a3

af=4.75e-05;                  %damage characteristic curve, af param
bf=0.85032;                   %damage characteristic parameter, bf param
alpha=4.31;                   %alpha value

T=-10;                        % temperature at which test is simulated

Element(:,5)=0.9999;          %ininital C value

%****************************** Assign initial paramweters
%*************************************************************************
Ymin=min(Node(:,3));
Ymax=max(Node(:,3));

r=(Ymax-Ymin)*0.5;              % radius
Teta=6.4/r;                     % Loading width = 12.8 mm
YNeg=Ymin+r*(1-cos(Teta));      % Max height of vertical boundry condition
YPos=Ymax-r*(1-cos(Teta));      % Min height of loading band

% ************* slave dof at which force is applied******************

IndexTop=find(Node(:,3)>=YPos); 
IndexBot=find(Node(:,3)<=YNeg); 

SlaveDof=Node(IndexTop,1);          % slave dof at which loading is applied
SlaveDof=2*sort(SlaveDof,'ascend'); 

% *********** horizontal boundry condition at which movement is restrained

Index_Hor=find(Node(:,2)==min(Node(:,2)));
Index_Hor=sort(2*Index_Hor-1,'ascend');
BC=sort(cat(1,2*IndexBot,Index_Hor),'ascend');

TempNode=sort(cat(1,SlaveDof,BC),'ascend');
TotalDof=(1:2*size(Node,1))';
[row,~]=find(repmat(TotalDof,1,length(TempNode))==repmat(TempNode',size(TotalDof,1),1));
row=sort(row,'descend');

MasterDof=TotalDof;
MasterDof(row)=[];

Node(:,2)=Node(:,2)/1000;
Node(:,3)=Node(:,3)/1000;

end

